package com.roi.museum;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;

public class GeneralAdmissionTicket {
    private LocalDateTime purchaseDateTime;
	private final long ONE_DAY = 24 * 60 * 60;

	public GeneralAdmissionTicket(LocalDateTime purchaseDateTime) {
		super();
		this.purchaseDateTime = purchaseDateTime;
	}
    
    public boolean isValid(Exhibit e, LocalDateTime visitDateTime){
    	boolean permanent = e.isPermanent();
    	boolean withinOneDay = isTimeDifferenceLessThan(purchaseDateTime, visitDateTime, ONE_DAY);

       	return permanent && withinOneDay;
    }

	private static boolean isTimeDifferenceLessThan(LocalDateTime firstDateTime, LocalDateTime secondDateTime, long maxDiff) {
		ZoneOffset offset = ZoneId.systemDefault().getRules().getStandardOffset(Instant.now());
    	
    	Instant firstInstant = firstDateTime.toInstant(offset);
    	Instant secondInstant = secondDateTime.toInstant(offset);
    	
    	long timeDiff =  secondInstant.getEpochSecond() - firstInstant.getEpochSecond();
		return timeDiff > 0 && timeDiff < maxDiff;
	}

}
