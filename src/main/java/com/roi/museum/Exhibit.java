package com.roi.museum;

public class Exhibit {
	
	private String name;
	private int cost;
	private boolean permanent;
	
	public Exhibit(String name, int cost, boolean permanent) {
		super();
		this.name = name;
		this.cost = cost;
		this.permanent = permanent;
	}
	
	
	

	public String getName() {
		return name;
	}




	public int getCost() {
		return cost;
	}




	public boolean isPermanent() {
		return permanent;
	}




	@Override
	public String toString() {
		return "Exhibit [name=" + name + ", cost=" + cost + ", permanent=" + permanent + "]";
	}
	
	

}
