package com.roi.museum;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class GeneralAdmissionTicketTest {

	private LocalDateTime purchaseDateTime;
	private LocalDateTime visitDateTime;

	@Before
	public void setup() {
		purchaseDateTime = LocalDateTime.of(2017, Month.SEPTEMBER, 15, 15, 0, 0);
		visitDateTime = LocalDateTime.of(2017, Month.SEPTEMBER, 15, 16, 0, 0);
	}
	
	@After
	public void teardown(){
		purchaseDateTime = null;
		visitDateTime = null;
	}
	
	@Test
	public void immediateEntryToPermanentExhibit() {

		Exhibit exhibit = new Exhibit("test", 300, true);

		GeneralAdmissionTicket ticket = new GeneralAdmissionTicket(purchaseDateTime);
		assertTrue(ticket.isValid(exhibit, visitDateTime));
	}
	
	@Test
	public void immediateEntryToVisitingExhibit() {
		
		Exhibit exhibit = new Exhibit("test", 300, false);

		GeneralAdmissionTicket ticket = new GeneralAdmissionTicket(purchaseDateTime);
		assertFalse(ticket.isValid(exhibit, visitDateTime));
	}
	
	@Test
	public void entryNextMonthToPermanentExhibit() {
		LocalDateTime nextMonthDateTime = LocalDateTime.of(2017,  Month.OCTOBER, 15, 15, 0, 0);
		Exhibit exhibit = new Exhibit("test", 300, true);
		GeneralAdmissionTicket ticket = new GeneralAdmissionTicket(purchaseDateTime);

		assertFalse( ticket.isValid(exhibit, nextMonthDateTime) );
	}
	
	@Test
	public void entryBeforePurchaseDateToPermanentExhibit() {
		LocalDateTime beforePurchaseDateTime = LocalDateTime.of(2017, Month.AUGUST, 15, 15, 0, 0);
		Exhibit exhibit = new Exhibit("test", 300, true);
		GeneralAdmissionTicket ticket = new GeneralAdmissionTicket(purchaseDateTime);
		assertFalse(ticket.isValid(exhibit, beforePurchaseDateTime));
	}

}
